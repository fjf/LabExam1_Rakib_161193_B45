Lab Exam1 Practical
--------------------

Part 1
-------

Task 1: Write a HTML file to Draw Chess board by using CSS (without table).

Task 2: Write chat.php code, so that one visitor may input their Chat Nick to a text box, 
and press the submit button to enter the main chatting window. 
Chat window should show chat history of all users, as well as chat text submission form. 

Part 2 
-------

Task 1: Dynamic Chessboard by using condition and loops, but just 1 div structure. 

Task 2: Multiple file upload by using one input field, 
and moved those files to a specific directory by using conditions and loops.
