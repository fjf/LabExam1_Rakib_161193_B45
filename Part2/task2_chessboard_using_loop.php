<!DOCTYPE html>
<html>
<head>
    <title></title>
    <meta charset=UTF-8">
</head>

<body>
<h3>ChessBoard Using Loop</h3>

<?php
session_start();
?>

<table width="<?php $_SESSION['number'].'* 30px';?>" cellspacing="0px" cellpadding="0px" border="1px">
    <?php
    if(isset($_POST['number'])) {
        $_SESSION['number'] = $_POST['number'];
    }

    if(isset($_SESSION['number'])) {
        for ($row = 1; $row <= $_SESSION['number']; $row++) {
            echo "<tr>";

            for ($col = 1; $col <= $_SESSION['number']; $col++) {
                $total = $row + $col;

                if ($total % 2 == 0) {
                    echo "<td height=30px width=30px bgcolor='papayawhip'></td>";
                }
                else {
                    echo "<td height=30px width=30px bgcolor=#000000></td>";
                }
            }
            echo "</tr>";
        }

        if(isset($_POST['dsconnect'])){
            session_destroy();
        }
        echo '
                <form action="" method="post">
                    <input type="submit" name="dsconnect" value="disconnect">
                </form>';
    }
    elseif(!isset($_SESSION['number'])){
        echo '
            <div class="number">
                <form action="" method="post" id="numberLogin">
                    <input type="text" name="number" placeholder="Enter chat-b44 number" required><br>
                    <input type="submit" value="Submit" name="submit">
                </form>
            </div>
        ';}
    ?>
</table>

</body>
</html>
