<html lang="en">
<head>
    <title>Chat On</title>
    <meta charset="UTF-8">
    <link href="style.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
</head>

<body>
<div class='header'>
    <h1>Welcome to Private chat</h1>
</div>

<div class='main'>

<?php
session_start();
//immediately saving $_POST['chatname'] to $_SESSION['chatname']
if(isset($_POST['chatName'])) {
    $_SESSION['chatName'] = $_POST['chatName'];
}

//immediately saving reply text to the chatHistory.txt file in append mode from $_POST['replyText']
if(isset($_POST['replyText'])) {
    $replyText = $_SESSION['chatName'].": ".$_POST['replyText']."\n";
    file_put_contents("chatHistory.txt",$replyText,FILE_APPEND);
}

function inputChatName(){
    echo '
    <div class="userscreen">
        <form action="" method="post">
            <input type="text" class="input-user" name="chatName" placeholder="ENTER YOUR NAME HERE">
            <input type="submit" class="btn btn-user" value="START CHAT" >
        </form>
    </div>
        ';
}

function showChatHistoryBox(){
    $chatHistory = file_get_contents("chatHistory.txt");
    echo "
          <textarea spellcheck='false' id='chatBox'> $chatHistory</textarea>
          <input type='checkbox' id='chkAutoReload' checked> 
         ";
}

function inputChatReply(){
    echo '
    <div id="result"></div>
    <div class="chatcontrols">
        <form action="" method="post">
            <input style="..." type="text" name="replyText" class="btn-send" autocomplete="off"
            placeholder="ENTER CHAT HERE">
            <input type="submit" name="send" id="send" class="btn btn-send" value="Send">
        </form>
        ';
}

if(!isset($_SESSION['chatName'])){
    inputChatName();
}
else{
    echo '<chat-b44 href="logoff.php">LOGOUT</chat-b44><br>';
    showChatHistoryBox();
    inputChatReply();
}
?>

<script>
    $(document).ready(
        function() {
            setInterval(function() {
                $("#chatBox").load("chatHistory.txt"); //it will make ajax call to chatHistory.txt,
                                  // and get output from that script and load to #chatbox textarea
            if(document.getElementById('chkAutoReload').checked ){
                var textarea = document.getElementById('chatbox');
                textarea.scrollTop = textarea.scrollHeight;
            }, 1000)
        }
    );
    var textarea = document.getElementById('chatbox');
    textarea.scrollTop = textarea.scrollHeight;
</script>
</div>

</body>
</html>
